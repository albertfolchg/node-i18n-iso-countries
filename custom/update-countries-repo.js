const fs = require('fs');

const countriesToKeepJson = JSON.parse(fs.readFileSync('./custom/countries-to-keep.json').toString());
const countriesToKeep = Object.keys(countriesToKeepJson.countries); // AF, AL, AD, ES...

const langsPath = './langs/';
fs.readdirSync(langsPath).forEach((langFileName) => {
    const fullLangPath = `${langsPath}${langFileName}`;
    const countryJson = JSON.parse(fs.readFileSync(fullLangPath).toString());
    const countryJsonFiltered = {
        ...countryJson,
        countries: Object.keys(countryJson.countries)
            .filter(twoCharCountry => countriesToKeep.includes(twoCharCountry))
            .reduce((obj, key) => {
                return {
                    ...obj,
                    [key]: countryJson.countries[key]
                };
            }, {}),
    };
    fs.writeFileSync(fullLangPath, JSON.stringify(countryJsonFiltered, null, 2));
});